# BurnedHTTPServer

Another cross-platform HTTP Server loosely based on FireHTTPServer. Written in Haxe and
designed to be as cross-platform as possible, running in the Neko VM, BurnedHTTPServer
also can be scripted in Haxe through `hscript`.